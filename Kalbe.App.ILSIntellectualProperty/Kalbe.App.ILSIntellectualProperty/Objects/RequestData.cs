﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

//reference
using System.ComponentModel.DataAnnotations;
namespace Kalbe.App.ILSIntellectualProperty.Api.Objects
{
    public partial class HeaderDetailData
    {
        public RequestHeaderData RequestHeader { get; set; } = new RequestHeaderData();
        public TrademarkDetail TrademarkDetail { get; set; } = new TrademarkDetail();
        public IEnumerable<TrademarkClassDetail> TrademarkClassDetail { get; set; } = new List<TrademarkClassDetail>();
        public PatentDetail PatentDetail { get; set; } = new PatentDetail();
        public IndustrialDesignDetail IndustrialDesignDetail { get; set; } = new IndustrialDesignDetail();
        public IEnumerable<IndustrialDesignClassDetail> IndustrialDesignClassDetail { get; set; } = new List<IndustrialDesignClassDetail>();
        public CopyrightDetail CopyrightDetail { get; set; } = new CopyrightDetail();
        public IEnumerable<AttachmentDetail> AttachmentDetail { get; set; } = new List<AttachmentDetail>();
    }
    public partial class RequestHeaderData
    {
        public string DocNo { get; set; }
        public string ReqNo { get; set; }
        public string RequestorName { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string Type { get; set; }
        public string ExistingLibrary { get; set; }
        public string LastReqNoLibrary { get; set; }
        public string LibraryDocNo { get; set; }
        public bool NA { get; set; }
        public string IntellectualPropertyType { get; set; }
        public string EntityIPOwner { get; set; }
        public string EntityIPOwnerNew { get; set; }
        public string Notes { get; set; }
        public string LegalPIC { get; set; }
        public string LegalPICName { get; set; }
        public string RegistrationNo { get; set; }
        public string Consultant { get; set; }
        public string Status { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public bool IsNewEntityIPOwner { get; set; }
        public bool IsNewAttachment { get; set; }
    }
}
