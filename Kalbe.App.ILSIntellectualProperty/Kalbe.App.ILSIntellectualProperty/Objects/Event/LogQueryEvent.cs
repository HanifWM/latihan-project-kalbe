﻿namespace Kalbe.App.ILSIntellectualProperty.Api.Objects.Event
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    //reference
    using Kalbe.Library.Message.Events;

    public class LogQueryEvent : Event
    {
        public LogQueryEvent()
        {
            this.Name = "LogQuery";
            this.TimeStamp = DateTime.Now;
        }

        public string SystemCode { get; set; }
        public string ModuleCode { get; set; }
        public string DocumentNumber { get; set; }
        public string Query { get; set; }
        public string QueryType { get; set; }
        public string CreatedBy { get; set; }
    }
}
