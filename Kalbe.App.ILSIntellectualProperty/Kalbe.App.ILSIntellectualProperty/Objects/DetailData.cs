﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

//reference
using System.ComponentModel.DataAnnotations;
namespace Kalbe.App.ILSIntellectualProperty.Api.Objects
{
    public partial class TrademarkDetail
    {
        public Guid ID { get; set; }
        public string DocNo { get; set; }
        public string TrademarkName { get; set; }
        public string TrademarkNameNew { get; set; }
        public string TrademarkType { get; set; }
        public string TrademarkTypeNew { get; set; }
        public string Territory { get; set; }
        public string TerritoryNew { get; set; }
        public string DataSource { get; set; }
        public string DataSourceNew { get; set; }
        public bool IsNewName { get; set; }
        public bool IsNewType { get; set; }
        public bool IsNewTerritory { get; set; }
        public bool IsNewDataSource { get; set; }
        public bool IsNewClass { get; set; }
    }

    public partial class TrademarkClassDetail
    {
        public Guid ID { get; set; }
        public Guid TrademarkID { get; set; }
        public string ClassOldNew { get; set; }
        public string TrademarkClass { get; set; }
        public string TrademarkSpecification { get; set; }
    }

    public partial class PatentDetail
    {
        public Guid ID { get; set; }
        public string DocNo { get; set; }
        public string PatentName { get; set; }
        public string PatentNameNew { get; set; }
        public string PatentDetails { get; set; }
        public string PatentDetailsNew { get; set; }
        public string PatentClass { get; set; }
        public string PatentClassNew { get; set; }
        public string Territory { get; set; }
        public string TerritoryNew { get; set; }
        public string DataSource { get; set; }
        public string DataSourceNew { get; set; }
        public bool IsNewName { get; set; }
        public bool IsNewDetails { get; set; }
        public bool IsNewClass { get; set; }
        public bool IsNewTerritory { get; set; }
        public bool IsNewDataSource { get; set; }
    }

    public partial class IndustrialDesignDetail
    {
        public Guid ID { get; set; }
        public string DocNo { get; set; }
        public string IndustrialDesignName { get; set; }
        public string IndustrialDesignNameNew { get; set; }
        public string IndustrialDesignDetails { get; set; }
        public string IndustrialDesignDetailsNew { get; set; }
        public string Territory { get; set; }
        public string TerritoryNew { get; set; }
        public string DataSource { get; set; }
        public string DataSourceNew { get; set; }
        public bool IsNewName { get; set; }
        public bool IsNewDetails { get; set; }
        public bool IsNewTerritory { get; set; }
        public bool IsNewDataSource { get; set; }
        public bool IsNewClass { get; set; }
    }

    public partial class IndustrialDesignClassDetail
    {
        public Guid ID { get; set; }
        public Guid IndustrialDesignID { get; set; }
        public string ClassOldNew { get; set; }
        public string IndustrialDesignClass { get; set; }
        public string IndustrialDesignSpecification { get; set; }
    }

    public partial class CopyrightDetail
    {
        public Guid ID { get; set; }
        public string DocNo { get; set; }
        public string CopyrightName { get; set; }
        public string CopyrightNameNew { get; set; }
        public string CopyrightDetails { get; set; }
        public string CopyrightDetailsNew { get; set; }
        public string Territory { get; set; }
        public string TerritoryNew { get; set; }
        public string DataSource { get; set; }
        public string DataSourceNew { get; set; }
        public bool IsNewName { get; set; }
        public bool IsNewDetails { get; set; }
        public bool IsNewTerritory { get; set; }
        public bool IsNewDataSource { get; set; }
    }

    public class AttachmentDetail
    {
        public Guid ID { get; set; }
        public string DocNo { get; set; }
        public string AttachmentOldNew { get; set; }
        public string Type { get; set; }
        public string FileName { get; set; }
        public string FileDescription { get; set; }
        public string FileLocation { get; set; }
        public string Date { get; set; }
        public bool IsFromMaster { get; set; }
    }
}
