﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//reference
using Kalbe.App.ILSIntellectualProperty.Api.Interfaces;
using Kalbe.App.ILSIntellectualProperty.Api.Objects.Event;
using Kalbe.Library.Message.Bus;
using System.Text.Json;
namespace Kalbe.App.ILSIntellectualProperty.Api.Services
{
    public partial class EventService : IEventService
    {
        private readonly IEventBus _eventBus;

        public EventService(IEventBus eventBus)
        {
            this._eventBus = eventBus;
        }

        public void LogQuery(string query, string commandType, object parameters)
        {
            var queryEvent = new LogQueryEvent()
            {
                SystemCode = "ILS",
                ModuleCode = "REQ-IP",
                DocumentNumber = "",
                Query = query + "; Parameters: " + (parameters is null ? "" : JsonSerializer.Serialize(parameters)),
                QueryType = commandType
            };
            _eventBus.Publish(queryEvent);
        }

        public void LogError(string methodCaller, string message, string stackTrace)
        {
            var errorEvent = new LogErrorEvent()
            {
                SystemCode = "ILS",
                ModuleCode = "REQ-IP",
                DocumentNumber = "",
                ErrorMessage = "Method:" + methodCaller + "; message: " + message + "; stack trace: " + stackTrace,
                CreatedBy = "System"
            };
            _eventBus.Publish(errorEvent);
        }
    }
}
