﻿namespace Kalbe.App.ILSIntellectualProperty.Api.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    //reference
    using Kalbe.Library.Data.Interfaces;
    using Kalbe.Library.Data.Services;
    using Kalbe.Library.Message.Bus;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Kalbe.App.ILSIntellectualProperty.Api.Interfaces;

    public static class ServiceRegistration
    {
        public static IServiceCollection RegisterServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IEventService, EventService>();
            services.AddScoped<IDataService, SqlDataService>();

            services.AddScoped<IRequestService, RequestService>();

            services.AddScoped<IEventBus, RabbitMQBus>();
            services.AddSingleton<IEventBus, RabbitMQBus>(sp =>
            {
                var scopeFactory = sp.GetRequiredService<IServiceScopeFactory>();
                return new RabbitMQBus(configuration, scopeFactory);
            });

            return services;
        }
    }
}
