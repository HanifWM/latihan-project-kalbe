﻿namespace Kalbe.App.ILSIntellectualProperty.Api.Services
{
    using Kalbe.App.ILSIntellectualProperty.Api.Interfaces;
    using Kalbe.App.ILSIntellectualProperty.Api.Objects;
    using Kalbe.Library.Data.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Threading.Tasks;

    public partial class RequestService : IRequestService
    {
        private readonly IDataService _dataService;
        private readonly IEventService _eventService;

        #region PRIVATE

        public static object UpdateStatus(RequestHeaderData request)
        {
            var result = new
            {
                DocNo = request.DocNo,
                Status = request.Status,
                UpdatedBy = request.UpdatedBy
            };

            return result;
        }

        #endregion

        public RequestService(IDataService dataService, IEventService eventService)
        {
            this._dataService = dataService;
            this._eventService = eventService;
        }

        public async Task<IEnumerable<RequestHeaderData>> GetAllAsync()
        {
            try
            {
                var result = await _dataService.GetMany<RequestHeaderData>("sp_GetAllIntellectualProperty", CommandType.StoredProcedure);
                _eventService.LogQuery("sp_GetAllIntellectualProperty", "StoredProcedure", null);
                return result;
            }
            catch (Exception ex)
            {
                _eventService.LogError("GetAllAsync", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public async Task<IEnumerable<RequestHeaderData>> GetAllByCreatorAsync(string Creator)
        {
            try
            {
                var result = await _dataService.GetMany<RequestHeaderData>("sp_GetAllByCreator", new { Creator }, CommandType.StoredProcedure);
                _eventService.LogQuery("sp_GetAllByCreator", "StoredProcedure", Creator);
                return result;
            }
            catch (Exception ex)
            {
                _eventService.LogError("sp_GetAllByCreator", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public async Task<HeaderDetailData> GetByDocNoAsync(string DocNo)
        {
            try
            {
                var result = new HeaderDetailData();

                // GET HEADER
                result.RequestHeader = await _dataService.GetOne<RequestHeaderData>("sp_GetOneIntellectualProperty", new { DocNo }, CommandType.StoredProcedure);
                _eventService.LogQuery("sp_GetOneIntellectualProperty", "StoredProcedure", DocNo);

                // GET ATTACHMENT
                result.AttachmentDetail = await _dataService.GetMany<AttachmentDetail>("sp_GetDetailAttachment", new { DocNo }, CommandType.StoredProcedure);
                _eventService.LogQuery("sp_GetDetailAttachment", "StoredProcedure", DocNo);

                // GET DETAIL TRADEMARK
                result.TrademarkDetail = await _dataService.GetOne<TrademarkDetail>("sp_GetDetailTrademark", new { DocNo }, CommandType.StoredProcedure);
                _eventService.LogQuery("sp_GetDetailTrademark", "StoredProcedure", DocNo);
                if (result.TrademarkDetail != null)
                {
                    // GET DETAIL TRADEMARK CLASS
                    result.TrademarkClassDetail = await _dataService.GetMany<TrademarkClassDetail>("sp_GetDetailTrademarkClass", new { TrademarkID = result.TrademarkDetail.ID }, CommandType.StoredProcedure);
                    _eventService.LogQuery("sp_GetDetailTrademarkClass", "StoredProcedure", DocNo);
                }

                // GET DETAIL PATENT
                result.PatentDetail = await _dataService.GetOne<PatentDetail>("sp_GetDetailPatent", new { DocNo }, CommandType.StoredProcedure);
                _eventService.LogQuery("sp_GetDetailPatent", "StoredProcedure", DocNo);

                // GET DETAIL INDUSTRIAL DESIGN
                result.IndustrialDesignDetail = await _dataService.GetOne<IndustrialDesignDetail>("sp_GetDetailIndustrialDesign", new { DocNo }, CommandType.StoredProcedure);
                _eventService.LogQuery("sp_GetDetailIndustrialDesign", "StoredProcedure", DocNo);
                if (result.IndustrialDesignDetail != null)
                {
                    // GET DETAIL INDUSTRIAL DESIGN CLASS
                    result.IndustrialDesignClassDetail = await _dataService.GetMany<IndustrialDesignClassDetail>("sp_GetDetailIndustrialDesignClass", new { IndustrialDesignID = result.IndustrialDesignDetail.ID }, CommandType.StoredProcedure);
                    _eventService.LogQuery("sp_GetDetailIndustrialDesignClass", "StoredProcedure", DocNo);
                }

                // GET DETAIL COPYRIGHT
                result.CopyrightDetail = await _dataService.GetOne<CopyrightDetail>("sp_GetDetailCopyright", new { DocNo }, CommandType.StoredProcedure);
                _eventService.LogQuery("sp_GetDetailCopyright", "StoredProcedure", DocNo);

                return result;
            }
            catch (Exception ex)
            {
                _eventService.LogError("GetByIdAsync", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public async Task<RequestHeaderData> GetOneByLibraryDocNoAsync(string libraryDocNo)
        {
            try
            {
                var result = new RequestHeaderData();
                var query = "SELECT * FROM t_IntellectualProperty WHERE LibraryDocNo = @libraryDocNo ORDER BY CreatedDate DESC";

                result = await _dataService.GetOne<RequestHeaderData>(query, new { libraryDocNo }, CommandType.Text);
                _eventService.LogQuery(query, "Text", libraryDocNo);

                return result;
            }
            catch (Exception ex)
            {
                _eventService.LogError("GetOneByLibraryDocNoAsync", ex.Message, ex.StackTrace);
                throw;
            }
        }
        public async Task<RequestHeaderData> GetOneByRegNoAsync(string regNo)
        {
            try
            {
                var result = new RequestHeaderData();
                var query = "SELECT * FROM t_IntellectualProperty WHERE ExistingLibrary = @regNo ORDER BY CreatedDate DESC";

                result = await _dataService.GetOne<RequestHeaderData>(query, new { regNo }, CommandType.Text);
                _eventService.LogQuery(query, "Text", regNo);

                return result;
            }
            catch (Exception ex)
            {
                _eventService.LogError("GetOneByRegNoAsync", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public async Task<HeaderDetailData> SaveAsync(HeaderDetailData _data)
        {
            try
            {
                // GENERATE DocNo
                var objDocNo = await _dataService.GetScalar("sp_GenerateDocNo", null, false, CommandType.StoredProcedure);
                _eventService.LogQuery("sp_GenerateDocNo", "StoredProcedure", null);
                var DocNo = objDocNo.ToString();

                // GENERATE ReqNo
                var objReqNo = await _dataService.GetScalar("sp_GenerateReqNo", new { DocNo, _data.RequestHeader.CompanyCode }, false, CommandType.StoredProcedure);
                _eventService.LogQuery("sp_GenerateReqNo", "StoredProcedure", DocNo);
                var ReqNo = objReqNo.ToString();

                _dataService.BeginTransaction();
                _eventService.LogQuery("BeginTransaction", "Text", null);

                // SAVE HEADER
                _data.RequestHeader.DocNo = DocNo;
                _data.RequestHeader.ReqNo = ReqNo;
                await _dataService.ExecuteNonQuery("sp_SaveIntellectualProperty", _data.RequestHeader, true, CommandType.StoredProcedure);
                _eventService.LogQuery("sp_SaveIntellectualProperty", "StoredProcedure", _data.RequestHeader);

                // SAVE ATTACHMENT
                foreach (var item in _data.AttachmentDetail)
                {
                    item.DocNo = DocNo;
                    await _dataService.ExecuteNonQuery("sp_SaveDetailAttachment", item, true, CommandType.StoredProcedure);
                    _eventService.LogQuery("sp_SaveDetailAttachment", "StoredProcedure", item);
                }

                switch (_data.RequestHeader.IntellectualPropertyType)
                {
                    case "TRADEMARK":
                        // SAVE DETAIL TRADEMARK
                        _data.TrademarkDetail.ID = Guid.NewGuid();
                        _data.TrademarkDetail.DocNo = DocNo;
                        await _dataService.ExecuteNonQuery("sp_SaveDetailTrademark", _data.TrademarkDetail, true, CommandType.StoredProcedure);
                        _eventService.LogQuery("sp_SaveDetailTrademark", "StoredProcedure", _data.TrademarkDetail);
                        foreach (var item in _data.TrademarkClassDetail)
                        {
                            item.TrademarkID = _data.TrademarkDetail.ID;
                            await _dataService.ExecuteNonQuery("sp_SaveDetailTrademarkClass", item, true, CommandType.StoredProcedure);
                            _eventService.LogQuery("sp_SaveDetailTrademarkClass", "StoredProcedure", item);
                        }
                        break;
                    case "PATENT":
                        // SAVE DETAIL PATENT
                        _data.PatentDetail.DocNo = DocNo;
                        await _dataService.ExecuteNonQuery("sp_SaveDetailPatent", _data.PatentDetail, true, CommandType.StoredProcedure);
                        _eventService.LogQuery("sp_SaveDetailPatent", "StoredProcedure", _data.PatentDetail);
                        break;
                    case "INDUSTRIAL DESIGN":
                        // SAVE DETAIL INDUSTRIAL DESIGN
                        _data.IndustrialDesignDetail.ID = Guid.NewGuid();
                        _data.IndustrialDesignDetail.DocNo = DocNo;
                        await _dataService.ExecuteNonQuery("sp_SaveDetailIndustrialDesign", _data.IndustrialDesignDetail, true, CommandType.StoredProcedure);
                        _eventService.LogQuery("sp_SaveDetailIndustrialDesign", "StoredProcedure", _data.IndustrialDesignDetail);
                        foreach (var item in _data.IndustrialDesignClassDetail)
                        {
                            item.IndustrialDesignID = _data.IndustrialDesignDetail.ID;
                            await _dataService.ExecuteNonQuery("sp_SaveDetailIndustrialDesignClass", item, true, CommandType.StoredProcedure);
                            _eventService.LogQuery("sp_SaveDetailIndustrialDesignClass", "StoredProcedure", item);
                        }
                        break;
                    case "COPYRIGHT":
                        // SAVE DETAIL COPYRIGHT
                        _data.CopyrightDetail.DocNo = DocNo;
                        await _dataService.ExecuteNonQuery("sp_SaveDetailCopyright", _data.CopyrightDetail, true, CommandType.StoredProcedure);
                        _eventService.LogQuery("sp_SaveDetailCopyright", "StoredProcedure", _data.CopyrightDetail);
                        break;
                    default:
                        break;
                }

                _dataService.CommitTransaction();
                _eventService.LogQuery("CommitTransaction", "Text", null);

                _data = await GetByDocNoAsync(DocNo);

                return _data;
            }
            catch (Exception ex)
            {
                _dataService.RollbackTransaction();
                _eventService.LogQuery("RollbackTransaction", "Text", null);
                _eventService.LogError("Save", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public async Task<bool> UpdateAsync(HeaderDetailData _data)
        {
            try
            {
                _dataService.BeginTransaction();
                _eventService.LogQuery("BeginTransaction", "Text", null);

                // UPDATE HEADER
                var DocNo = _data.RequestHeader.DocNo;
                var result = await _dataService.ExecuteNonQuery("sp_UpdateIntellectualProperty", _data.RequestHeader, true, CommandType.StoredProcedure);
                _eventService.LogQuery("sp_UpdateIntellectualProperty", "StoredProcedure", _data.RequestHeader);

                // DELETE DETAILS
                await _dataService.ExecuteNonQuery("sp_HardDeleteDetails", new { DocNo }, true, CommandType.StoredProcedure);
                _eventService.LogQuery("sp_HardDeleteDetails", "StoredProcedure", DocNo);

                #region SAVE DETAIL
                // SAVE ATTACHMENT
                foreach (var item in _data.AttachmentDetail)
                {
                    item.DocNo = DocNo;
                    await _dataService.ExecuteNonQuery("sp_SaveDetailAttachment", item, true, CommandType.StoredProcedure);
                    _eventService.LogQuery("sp_SaveDetailAttachment", "StoredProcedure", item);
                }

                switch (_data.RequestHeader.IntellectualPropertyType)
                {
                    case "TRADEMARK":
                        // SAVE DETAIL TRADEMARK
                        _data.TrademarkDetail.ID = Guid.NewGuid();
                        _data.TrademarkDetail.DocNo = DocNo;
                        await _dataService.ExecuteNonQuery("sp_SaveDetailTrademark", _data.TrademarkDetail, true, CommandType.StoredProcedure);
                        _eventService.LogQuery("sp_SaveDetailTrademark", "StoredProcedure", _data.TrademarkDetail);
                        foreach (var item in _data.TrademarkClassDetail)
                        {
                            item.TrademarkID = _data.TrademarkDetail.ID;
                            await _dataService.ExecuteNonQuery("sp_SaveDetailTrademarkClass", item, true, CommandType.StoredProcedure);
                            _eventService.LogQuery("sp_SaveDetailTrademarkClass", "StoredProcedure", item);
                        }
                        break;
                    case "PATENT":
                        // SAVE DETAIL PATENT
                        _data.PatentDetail.DocNo = DocNo;
                        await _dataService.ExecuteNonQuery("sp_SaveDetailPatent", _data.PatentDetail, true, CommandType.StoredProcedure);
                        _eventService.LogQuery("sp_SaveDetailPatent", "StoredProcedure", _data.PatentDetail);
                        break;
                    case "INDUSTRIAL DESIGN":
                        // SAVE DETAIL INDUSTRIAL DESIGN
                        _data.IndustrialDesignDetail.ID = Guid.NewGuid();
                        _data.IndustrialDesignDetail.DocNo = DocNo;
                        await _dataService.ExecuteNonQuery("sp_SaveDetailIndustrialDesign", _data.IndustrialDesignDetail, true, CommandType.StoredProcedure);
                        _eventService.LogQuery("sp_SaveDetailIndustrialDesign", "StoredProcedure", _data.IndustrialDesignDetail);
                        foreach (var item in _data.IndustrialDesignClassDetail)
                        {
                            item.IndustrialDesignID = _data.IndustrialDesignDetail.ID;
                            await _dataService.ExecuteNonQuery("sp_SaveDetailIndustrialDesignClass", item, true, CommandType.StoredProcedure);
                            _eventService.LogQuery("sp_SaveDetailIndustrialDesignClass", "StoredProcedure", item);
                        }
                        break;
                    case "COPYRIGHT":
                        // SAVE DETAIL COPYRIGHT
                        _data.CopyrightDetail.DocNo = DocNo;
                        await _dataService.ExecuteNonQuery("sp_SaveDetailCopyright", _data.CopyrightDetail, true, CommandType.StoredProcedure);
                        _eventService.LogQuery("sp_SaveDetailCopyright", "StoredProcedure", _data.CopyrightDetail);
                        break;
                    default:
                        break;
                }
                #endregion

                _dataService.CommitTransaction();
                _eventService.LogQuery("CommitTransaction", "Text", null);

                return (result > 0);
            }
            catch (Exception ex)
            {
                _dataService.RollbackTransaction();
                _eventService.LogQuery("RollbackTransaction", "Text", null);
                _eventService.LogError("Update", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public async Task<bool> UpdateStatusAsync(RequestHeaderData request)
        {
            try
            {
                var paramHeader = UpdateStatus(request);

                var dataUpdate = await _dataService.ExecuteNonQuery("sp_UpdateStatusIP", paramHeader, false, CommandType.StoredProcedure);
                _eventService.LogQuery("sp_UpdateStatusIP", "StoredProcedure", paramHeader);

                return (dataUpdate > 0);
            }
            catch (Exception ex)
            {
                _eventService.LogError("Update", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public async Task<bool> DeleteAsync(string DocNo)
        {
            try
            {
                var data = await _dataService.ExecuteNonQuery("sp_DeleteIntellectualProperty", new { DocNo }, false, CommandType.StoredProcedure);
                _eventService.LogQuery("sp_DeleteIntellectualProperty", "StoredProcedure", DocNo);
                return (data > 0);
            }
            catch (Exception ex)
            {
                _eventService.LogError("Delete", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public async Task<bool> HardDeleteAsync(string DocNo)
        {
            try
            {
                _dataService.BeginTransaction();
                _eventService.LogQuery("BeginTransaction", "Text", null);

                await _dataService.ExecuteNonQuery("sp_HardDeleteDetails", new { DocNo }, true, CommandType.StoredProcedure);
                _eventService.LogQuery("sp_HardDeleteDetails", "StoredProcedure", DocNo);

                // DELETE HEADER
                var data = await _dataService.ExecuteNonQuery("sp_HardDeleteIntellectualProperty", new { DocNo }, true, CommandType.StoredProcedure);
                _eventService.LogQuery("sp_HardDeleteIntellectualProperty", "StoredProcedure", DocNo);

                _dataService.CommitTransaction();
                _eventService.LogQuery("CommitTransaction", "Text", null);

                return (data > 0);
            }
            catch (Exception ex)
            {
                _eventService.LogError("Delete", ex.Message, ex.StackTrace);
                throw;
            }
        }

        
    }
}