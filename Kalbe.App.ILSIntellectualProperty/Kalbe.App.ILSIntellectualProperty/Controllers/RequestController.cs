﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//reference
using Kalbe.App.ILSIntellectualProperty.Api.Interfaces;
using Kalbe.App.ILSIntellectualProperty.Api.Objects;
using System.Net;
using System.Web;

namespace Kalbe.App.ILSIntellectualProperty.Api.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public partial class RequestController : ControllerBase
    {
        private readonly IRequestService _request;

        public RequestController(IRequestService request)
        {
            this._request = request;
        }
        //Menampilkan semua data dari database
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RequestHeaderData>>> Get()
        {
            var result = await _request.GetAllAsync();
            return Ok(result);
        }
        //Menampilkan satu data dari database berdasarkan creator
        [HttpGet("creator/{Creator}")]
        public async Task<ActionResult<IEnumerable<RequestHeaderData>>> GetByCreator(string Creator)
        {
            var decodeCreator = WebUtility.UrlDecode(Creator);
            var result = await _request.GetAllByCreatorAsync(decodeCreator);
            return Ok(result);
        }
        //Menampilkan satu data dari database berdasarkan docNo
        [HttpGet("{docNo}")]
        public async Task<ActionResult<HeaderDetailData>> GetByDocNo(string docNo)
        {
            docNo = WebUtility.UrlDecode(docNo);
            var result = await _request.GetByDocNoAsync(docNo);
            return result;
        }
        //Menampilkan satu data dari database berdasarkan LibbraryDocNo
        public async Task<ActionResult<RequestHeaderData>> GetOneByLibraryDocNo(string libraryDocNo)
        {
            var encodeNo = WebUtility.UrlDecode(libraryDocNo);
            var result = await _request.GetOneByLibraryDocNoAsync(encodeNo);
            return result;
        }
        //Menampilkan satu data dari database berdasarkan regNo
        public async Task<ActionResult<RequestHeaderData>> GetOneByRegNo(string regNo)
        {
            var encodeNo = WebUtility.UrlDecode(regNo);
            var result = await _request.GetOneByRegNoAsync(encodeNo);
            return result;
        }
        //Membuat data baru ke database
        [HttpPost]
        public async Task<ActionResult<HeaderDetailData>> Save([FromBody]HeaderDetailData _data)
        {
            var result = await _request.SaveAsync(_data);
            return CreatedAtAction(nameof(GetByDocNo), new { docNo = _data.RequestHeader.DocNo }, result);
        }
        //Mengupdate dari data yang sudah ada
        [HttpPut("{docNo}")]
        public async Task<ActionResult> Update(string docNo, [FromBody]HeaderDetailData _data)
        {
            docNo = WebUtility.UrlDecode(docNo);
            
            if(docNo != _data.RequestHeader.DocNo)
            {
                return BadRequest();
            }
            
            try
            {
                var result = await _request.UpdateAsync(_data);
            }

            catch
            {
                var oldData = await _request.GetByDocNoAsync(docNo);
                
                if(oldData == null)
                {
                    return NotFound();
                }

                else
                {
                    throw;
                }
            }
            return NoContent();
        }
        //Memperbarui status data berdasarkan DocNo
        [HttpPut("Status/{DocNo}")]
        public async Task<ActionResult> UpdateStatus(string DocNo, [FromBody]RequestHeaderData request)
        {
            string sDocNo = HttpUtility.UrlDecode(DocNo);

            if(sDocNo != request.DocNo)
            {
                return BadRequest();
            }

            try
            {
                var result = await _request.UpdateStatusAsync(request);
            }

            catch
            {
                var oldData = await _request.GetByDocNoAsync(DocNo);
                
                if(oldData == null)
                {
                    return NotFound();
                }

                else
                {
                    throw;
                }
            }
            return NoContent();
        }
        //Menghapus data dari database
        [HttpDelete("{docNo}")]
        public async Task<ActionResult> Delete(string docNo)
        {
            docNo = WebUtility.UrlDecode(docNo);
            var data = await _request.GetByDocNoAsync(docNo);
            
            if (data.RequestHeader == null)
            {
                return NotFound();
            }

            var result = await _request.DeleteAsync(docNo);
            return NoContent();
        }
        //Menghapus data dari database
        public async Task<ActionResult> HardDelete(string docNo)
        {
            docNo = WebUtility.UrlDecode(docNo);
            var data = await _request.GetByDocNoAsync(docNo);

            if(data == null)
            {
                return NotFound();
            }
            var result = await _request.HardDeleteAsync(docNo);
            return NoContent();
        }
    }
}
