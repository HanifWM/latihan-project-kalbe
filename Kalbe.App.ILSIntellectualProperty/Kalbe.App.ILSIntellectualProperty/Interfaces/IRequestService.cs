﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

//reference
using Kalbe.App.ILSIntellectualProperty.Api.Objects;

namespace Kalbe.App.ILSIntellectualProperty.Api.Interfaces
{
    public partial interface IRequestService
    {
        Task<IEnumerable<RequestHeaderData>> GetAllAsync();
        Task<IEnumerable<RequestHeaderData>> GetAllByCreatorAsync(string Creator);
        Task<HeaderDetailData> GetByDocNoAsync(string docNo);
        Task<RequestHeaderData> GetOneByLibraryDocNoAsync(string libraryDocNo);
        Task<RequestHeaderData> GetOneByRegNoAsync(string regNo);
        Task<HeaderDetailData> SaveAsync(HeaderDetailData _data);
        Task<bool> UpdateAsync(HeaderDetailData request);
        Task<bool> UpdateStatusAsync(RequestHeaderData request);
        Task<bool> DeleteAsync(string docNo);
        Task<bool> HardDeleteAsync(string docNo);
    }
}
