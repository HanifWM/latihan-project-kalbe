﻿namespace Kalbe.App.ILSIntellectualProperty.Api.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public partial interface IEventService
    {
        void LogQuery(string query, string commandType, object parameters);

        void LogError(string methodCaller, string message, string strackTrace);
    }
}
