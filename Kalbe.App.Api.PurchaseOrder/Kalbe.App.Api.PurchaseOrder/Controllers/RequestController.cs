﻿using Kalbe.App.Api.PurchaseOrder.Interfaces;
using Kalbe.App.Api.PurchaseOrder.Objects;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kalbe.App.Api.PurchaseOrder.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class RequestController : ControllerBase
    {
        private readonly IRequestService _request;

        public RequestController(IRequestService request)
        {
            this._request = request;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<DPelanggan>>> Get()
        {
            var result = await _request.GetAllAsync();
            return Ok(result);
        }

        [HttpGet("GetOneById/{Id}")]
        public async Task<ActionResult<DPelanggan>> GetOneById(int Id)
        {
            var result = await _request.GetOneByIdAsync(Id);
            return result;
        }

        [HttpPost]
        public async Task<ActionResult<DOrders>> Save([FromBody] DOrders _data)
        {
            var result = await _request.SaveAsync(_data);
            return result;
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(int Id)
        {
            var data = await _request.GetOneByIdAsync(Id);
            if(data.Id == null)
            {
                return NotFound();
            }

            var result = await _request.DeleteAsync(Id);
            return NoContent();
        }
    }
}
