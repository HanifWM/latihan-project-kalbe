﻿using Kalbe.App.Api.PurchaseOrder.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kalbe.App.Api.PurchaseOrder.Interfaces
{
    public partial interface IRequestService
    {
        Task<IEnumerable<DPelanggan>> GetAllAsync();

        Task<DPelanggan> GetOneByIdAsync(int Id);

        Task<DOrders> SaveAsync(DOrders _data/*, DSupplies[] _supplies*/);

        Task<bool> DeleteAsync(int Id);
    }
}
