﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Kalbe.App.Api.PurchaseOrder.Objects
{
    public partial class DbPurchaseOrderContext : DbContext
    {
        public DbPurchaseOrderContext()
        {
        }

        public DbPurchaseOrderContext(DbContextOptions<DbPurchaseOrderContext> options)
            : base(options)
        {
        }

        public virtual DbSet<DListBarang> DListBarang { get; set; }
        public virtual DbSet<DOrders> DOrders { get; set; }
        public virtual DbSet<DPelanggan> DPelanggan { get; set; }
        public virtual DbSet<DSupplies> DSupplies { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("data source=LAPTOP-9785S29N\\SQLEXPRESS;initial catalog=DbPurchaseOrder;persist security info=True;user id=sa;password=12345;MultipleActiveResultSets=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DListBarang>(entity =>
            {
                entity.ToTable("d_listBarang");

                entity.Property(e => e.BanyakBarang).HasColumnName("banyakBarang");

                entity.Property(e => e.HargaBarang).HasColumnName("hargaBarang");

                entity.Property(e => e.NamaBarang)
                    .HasColumnName("namaBarang")
                    .HasMaxLength(10)
                    .IsFixedLength();
            });

            modelBuilder.Entity<DOrders>(entity =>
            {
                entity.ToTable("d_Orders");

                entity.Property(e => e.CustomerId).HasColumnName("customerId");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("date");

                entity.Property(e => e.Departement)
                    .HasColumnName("departement")
                    .HasMaxLength(20)
                    .IsFixedLength();

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(20)
                    .IsFixedLength();

                entity.Property(e => e.ShipTo)
                    .HasColumnName("shipTo")
                    .HasMaxLength(20)
                    .IsFixedLength();

                entity.Property(e => e.Via)
                    .HasColumnName("via")
                    .HasMaxLength(20)
                    .IsFixedLength();

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.DOrders)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK_d_Orders_d_Pelanggan");
            });

            modelBuilder.Entity<DPelanggan>(entity =>
            {
                entity.ToTable("d_Pelanggan");

                entity.Property(e => e.Email)
                    .HasMaxLength(20)
                    .IsFixedLength();

                entity.Property(e => e.Name)
                    .HasMaxLength(20)
                    .IsFixedLength();

                entity.Property(e => e.Password)
                    .HasMaxLength(20)
                    .IsFixedLength();
            });

            modelBuilder.Entity<DSupplies>(entity =>
            {
                entity.ToTable("d_Supplies");

                entity.Property(e => e.BanyakBarang).HasColumnName("banyakBarang");

                entity.Property(e => e.HargaBarang).HasColumnName("hargaBarang");

                entity.Property(e => e.NamaBarang)
                    .HasColumnName("namaBarang")
                    .HasMaxLength(20)
                    .IsFixedLength();

                entity.Property(e => e.OrderId).HasColumnName("orderId");

                entity.Property(e => e.TotalBarang).HasColumnName("totalBarang");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.DSupplies)
                    .HasForeignKey(d => d.OrderId)
                    .HasConstraintName("FK_d_Supplies_d_Orders");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
