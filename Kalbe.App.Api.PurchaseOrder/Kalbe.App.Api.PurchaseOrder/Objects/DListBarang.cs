﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Kalbe.App.Api.PurchaseOrder.Objects
{
    public partial class DListBarang
    {
        public int Id { get; set; }
        public string NamaBarang { get; set; }
        public int? BanyakBarang { get; set; }
        public int? HargaBarang { get; set; }
    }
}
