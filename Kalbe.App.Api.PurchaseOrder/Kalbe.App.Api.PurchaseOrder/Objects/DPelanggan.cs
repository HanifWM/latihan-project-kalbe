﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Kalbe.App.Api.PurchaseOrder.Objects
{
    public partial class DPelanggan
    {
        public DPelanggan()
        {
            DOrders = new HashSet<DOrders>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public virtual ICollection<DOrders> DOrders { get; set; }
    }
}
