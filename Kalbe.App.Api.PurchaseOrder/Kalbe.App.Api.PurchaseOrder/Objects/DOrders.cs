﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Kalbe.App.Api.PurchaseOrder.Objects
{
    public partial class DOrders
    {
        public DOrders()
        {
            DSupplies = new HashSet<DSupplies>();
        }

        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public string ShipTo { get; set; }
        public string Via { get; set; }
        public string Email { get; set; }
        public DateTime? Date { get; set; }
        public string Departement { get; set; }

        public virtual DPelanggan Customer { get; set; }
        public virtual ICollection<DSupplies> DSupplies { get; set; }
    }
}
