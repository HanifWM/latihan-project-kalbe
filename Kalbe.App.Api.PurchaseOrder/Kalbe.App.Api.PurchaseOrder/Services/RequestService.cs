﻿using Kalbe.App.Api.PurchaseOrder.Interfaces;
using Kalbe.App.Api.PurchaseOrder.Objects;
using Kalbe.Library.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Kalbe.App.Api.PurchaseOrder.Services
{
    public partial class RequestService : IRequestService
    {
        private readonly IDataService _dataService;

        public RequestService(IDataService dataService)
        {
            this._dataService = dataService;
        }

        public async Task<IEnumerable<DPelanggan>> GetAllAsync()
        {
            try
            {
                var result = await _dataService.GetMany<DPelanggan>("sp_GetAllPurchaseOrder", CommandType.StoredProcedure);
                return result;
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        public async Task<DPelanggan> GetOneByIdAsync(int Id)
        {
            try
            {
                var result = await _dataService.GetOne<DPelanggan>("sp_GetOneById", Id, CommandType.StoredProcedure);
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<DOrders> SaveAsync(DOrders _data/*, DSupplies[] _supplies*/)
        {
            try
            {
                //Save Order
                await _dataService.ExecuteNonQuery("sp_SaveOrder", _data, true, CommandType.StoredProcedure);
               
                //Get Order Id
                var customerId = await _dataService.GetOne<DOrders>("sp_GetOneOrdersId", CommandType.StoredProcedure);

                //foreach (var item in _data.IndustrialDesignClassDetail)
                //{
                //    item.IndustrialDesignID = _data.IndustrialDesignDetail.ID;
                //    await _dataService.ExecuteNonQuery("sp_SaveDetailIndustrialDesignClass", item, true, CommandType.StoredProcedure);
                //    _eventService.LogQuery("sp_SaveDetailIndustrialDesignClass", "StoredProcedure", item);
                //}
                return _data;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<bool> DeleteAsync(int Id)
        {
            try
            {
                var _list = await _dataService.GetMany<DOrders>("sp_GetAllSuppliesByCustomerId", Id, CommandType.StoredProcedure);
                var data = await _dataService.ExecuteNonQuery("sp_DeleteOrders", Id, false, CommandType.StoredProcedure);
                foreach (var item in _list)
                {
                    await _dataService.ExecuteNonQuery("sp_DeleteSupplies", item, false, CommandType.StoredProcedure);
                }
                return (data > 0);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
 